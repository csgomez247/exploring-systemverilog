module CLA(output logic [2:0] carries, output logic cout, input logic [2:0] ps, gs, input logic cin);

    always_comb
        begin
            carries[0] = cin;
            carries[1] = gs[0] | ps[0] & cin;
            carries[2] = gs[1] | ps[1] & gs[0] | ps[1] & ps[0] & cin;
            cout = gs[2] | (ps[2] & gs[1]) | (ps[2] & ps[1] & gs[0]) | (ps[2] & ps[1] & ps[0] & cin);
        end // always_comb

endmodule // CLA

module ALU(output logic [8:0] out, input [8:0] a, b, input[3:0] opcode);

    logic cout;
    logic [8:0] fa9out, aa, bb;
    Full_Adder_9 fa9(aa, bb, 1'b0, fa9out, cout);

    logic cout1;
    logic [8:0] bbb, negplusout;
    Full_Adder_9 negplus1(9'b000000001, bbb, 1'b0, negplusout, cout1);

    always_comb
    begin
        case(opcode)
            4'b0000: out = a & b;
            4'b0001: out = a | b;
            4'b0010: out = ~a;
            4'b0011:
                begin
                    aa = a;
                    bb = b;
                    out = fa9out;
                end
            4'b0100: out = a;
            4'b0101:
                begin
                    logic [8:0] temp;
                    temp[8:1] = a[7:0];
                    temp[0] = 1'b0;
                    out = temp;
                end
            4'b0110:
                begin
                    logic [8:0] temp;
                    temp[7:0] = a[8:1];
                    temp[8] = 1'b0;
                    out = temp;
                end
            4'b0111:
                begin
                    aa = a;
                    bbb = ~b;
                    bb = negplusout;
                    out = fa9out;
                end
            4'b1000:
                begin
                    aa = a;
                    bb = opcode;
                    out = fa9out;
                end
            4'b1001:
                begin
                    aa = a;
                    bbb = ~opcode;
                    bb = negplusout;
                    out = fa9out;
                end
            4'b1010: out = opcode;

        endcase // case(opcode)
    end//always_comb

endmodule

module Full_Adder_9(input logic [8:0] a, b, input logic cin, output logic [8:0] out, output logic cout);

    logic cout1;
    Full_Adder_3 add1(a[2:0], b[2:0], cin, out[2:0], cout1);

    logic cout2;
    Full_Adder_3 add2(a[5:3], b[5:3], cout1, out[5:3], cout2);

    Full_Adder_3 add3(a[8:6], b[8:6], cout2, out[8:6], cout);

endmodule // Full_Adder_9

module Full_Adder_3(input logic [2:0] a, b, input logic cin, output logic [2:0] out, output logic cout);

    logic [2:0] carries, ps, gs;
    assign ps = a[2:0] ^ b[2:0];
    assign gs = a[2:0] & b[2:0];
    CLA cla3(carries, cout, ps, gs, cin);

    logic cout0;
    Full_Adder_1 add0(a[0], b[0], carries[0], out[0], cout0);

    logic cout1;
    Full_Adder_1 add1(a[1], b[1], carries[1], out[1], cout1);

    logic cout2;
    Full_Adder_1 add2(a[2], b[2], carries[2], out[2], cout2);

endmodule // Full_Adder_3

module Full_Adder_1(input logic a, b, cin, output logic out, cout);

    logic p, g;

    assign p = a ^ b;
    assign g = a & b;

    assign out = p ^ cin;
    assign cout = g | (p & cin);

endmodule // Full_Adder_1