module srlatch( input logic s, r,
                output logic q, qbar);

    always_latch
        if(s == 0 & r == 0)
        begin
            assign q = q;
            assign qbar = ~q;
        end

        else if(s == 0 & r == 1)
        begin
            assign q = 0;
            assign qbar = ~q;
        end

        else if(s == 1 & r == 0)
        begin
            assign q = 1;
            assign qbar = ~q;

        end

        else
        begin
            assign q = q;
            assign qbar = ~q;
        end


endmodule // srlatch