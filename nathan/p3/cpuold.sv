module cpu(input clock);

logic [21:0] RAMdata;
logic [8:0] a, b, data, Bdata;
logic [6:0][8:0] out;
logic [3:0] opcode;
logic PC_Reset;
logic [6:0][8:0] registerBus;
logic [9:0] Address;
logic [2:0] A, B, D;

RAM_Test ram(RAMdata, PC_Reset, Address, registerBus, clock);
PC counter(clock, PC_Reset, Address);
ALU al(data, a, b, opcode);
RegisterBank bank(clock, D, data, out);
Mux8 muxA(out, Bdata, A, a);
Mux8 muxB(out, Bdata, B, b);

assign opcode = RAMdata[21:18];
assign A = RAMdata[17:15];
assign B = RAMdata[14:12];
assign D = RAMdata[11:9];
assign Bdata = RAMdata[8:0];
endmodule

module PC(input clock, input PC_Reset, output logic [9:0] Address);

	always_ff@(posedge clock, posedge PC_Reset)
		if(PC_Reset)
			Address <= 0;
		else
			Address <= Address + 1;

endmodule

module Mux8(input logic [6:0][8:0] d, input logic [8:0] BData,
														 input logic [2:0] s, output logic [8:0] y);

	assign y = s[2] ? (s[1] ? (s[0] ? d[6] : d[5])
													: (s[0] ? d[4] : d[3]))
									: (s[1] ? (s[0] ? d[2] : d[1])
													: (s[0] ? d[0] : BData));

endmodule

module RegisterBank(input clock, input logic [2:0] D, input logic [8:0] data,
										output logic [6:0][8:0] out);

	logic [8:0] d0, d1, d2, d3, d4, d5, d6;

	Register r0(clock, d0, out[0]);
	Register r1(clock, d1, out[1]);
	Register r2(clock, d2, out[2]);
	Register r3(clock, d3, out[3]);
	Register r4(clock, d4, out[4]);
	Register r5(clock, d5, out[5]);
	Register r6(clock, d6, out[6]); 

	always_comb
		case(D)
			0: d0 <= data;
			1: d1 <= data;
			2: d2 <= data;
			3: d3 <= data;
			4: d4 <= data;
			5: d5 <= data;
			6: d6 <= data;
			default: ;
		endcase

endmodule

module Register(input clock, input logic [8:0] d, output logic[8:0] q);

	always_ff @(posedge clock)
		q <= d;

endmodule

module Decoder(input logic [2:0] D, output logic [6:0] out);

	always_comb
		case(D)
			0: out <= 7'b0000001;
			1: out <= 7'b0000010;
			2: out <= 7'b0000100;
			3: out <= 7'b0001000;
			4: out <= 7'b0010000;
			5: out <= 7'b0100000;
			6: out <= 7'b1000000;
			default: out <= 7'b0000000;
		endcase

endmodule
