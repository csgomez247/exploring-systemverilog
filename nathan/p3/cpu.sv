module cpu(input clock);

logic [21:0] RAMdata;
logic [8:0] a, b, data, datain;
logic [6:0][8:0] out;
logic [3:0] opcode;
logic PC_Reset;
logic [6:0][8:0] registerBus;
logic [9:0] Address;
logic [2:0] A, B, D;
logic [6:0] choose;

RAM_Test ram(RAMdata, PC_Reset, Address, out, clock);
PC counter(clock, PC_Reset, Address);
ALU al(data, a, b, opcode);
RegisterBank bank(clock, choose, data, out);
Decoder decode(D, choose, opcode);
Mux8 muxA(out, datain, A, a);
BMux8 muxB(out, datain, B, opcode, b);

	always_comb
		begin
			opcode <= RAMdata[21:18];
			A <= RAMdata[17:15];
			B <= RAMdata[14:12];
			D <= RAMdata[11:9];
			datain <= RAMdata[8:0];
		end
endmodule

module PC(input clock, input PC_Reset, output logic [9:0] Address);

	always_ff@(posedge clock, posedge PC_Reset)
		if(PC_Reset)
			Address <= 0;
		else
			Address <= Address + 1;

endmodule

module Mux8(input logic [6:0][8:0] d, input logic [8:0] BData,
														 input logic [2:0] s, output logic [8:0] y);

always@(s)
	assign y = s[2] ? (s[1] ? (s[0] ? BData : d[6])
													: (s[0] ? d[5] : d[4]))
									: (s[1] ? (s[0] ? d[3] : d[2])
													: (s[0] ? d[1] : d[0]));

endmodule

module BMux8(input logic [6:0][8:0] d, input logic [8:0] BData,
														 input logic [2:0] s, input logic [3:0] opcode, output logic [8:0] y);

always@(s, opcode)
if(opcode == 4'b1000 || opcode == 4'b1001 || opcode == 4'b1010)
	assign y = BData;
else
	assign y = s[2] ? (s[1] ? (s[0] ? BData : d[6])
													: (s[0] ? d[5] : d[4]))
									: (s[1] ? (s[0] ? d[3] : d[2])
													: (s[0] ? d[1] : d[0]));

endmodule


module RegisterBank(input clock, input logic [6:0] D, input logic [8:0] data,
										output logic [6:0][8:0] out);


	Register r0(clock, D[0], data, out[0]);
	Register r1(clock, D[1], data, out[1]);
	Register r2(clock, D[2], data, out[2]);
	Register r3(clock, D[3], data, out[3]);
	Register r4(clock, D[4], data, out[4]);
	Register r5(clock, D[5], data, out[5]);
	Register r6(clock, D[6], data, out[6]); 

endmodule

module Register(input clock, input check, input logic [8:0] d, output logic[8:0] q);

	always_ff @(negedge clock)
		if(check) q <= d;

endmodule

module Decoder(input logic [2:0] D, output logic [6:0] out, input logic [3:0] opcode);

	always@(D, opcode)
		if(opcode == 11)
			out <= 0;
		else
		case(D)
			0: begin out[0] <= 1; out[6:1] <= 0; end
			1: begin out[1] <= 1; out[0] <= 0; out[6:2] <= 0; end
			2: begin out[2] <= 1; out[1:0] <= 0; out[6:3] <= 0; end
			3: begin out[3] <= 1; out[2:0] <= 0; out[6:4] <=0; end
			4: begin out[4] <= 1; out[3:0] <= 0; out[6:5] <=0; end
			5: begin out[5] <= 1; out[4:0] <= 0; out[6] <=0; end
			6: begin out[6] <= 1; out[5:0] <= 0; end
			default: out <= 0;
		endcase

endmodule
