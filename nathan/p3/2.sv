module srlatch2(input logic s, r, output logic q, qbar);
	always@(s, r)
		begin
			if(s) 
				begin
					q <= 1;
					qbar <= 0;
				end
			else if(r)
				begin
					q <= 0;
					qbar <= 1;
				end
		end
endmodule

module srlatch(input logic s, r, output logic q, qbar);

			assign q = ~(r | qbar);
			assign qbar = ~(s | q);

endmodule
