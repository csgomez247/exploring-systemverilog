module ex4_38(input logic clk, reset, up, output logic [2:0] q);

    always_ff @(posedge clk or posedge reset)
    begin
        logic [2:0] s0 = 3'b000;
        logic [2:0] s1 = 3'b001;
        logic [2:0] s2 = 3'b011;
        logic [2:0] s3 = 3'b010;
        logic [2:0] s4 = 3'b110;
        logic [2:0] s5 = 3'b111;
        logic [2:0] s6 = 3'b101;
        logic [2:0] s7 = 3'b100;

        if(reset)
            begin
                q <= s0;
            end
        else if(up)
            begin
                case(q)
                    3'b000: q <= s1;
                    3'b001: q <= s2;
                    3'b011: q <= s3;
                    3'b010: q <= s4;
                    3'b110: q <= s5;
                    3'b111: q <= s6;
                    3'b101: q <= s7;
                    3'b100: q <= s0;
                    default: q <= q;
                endcase
            end // else
        else if(~up)// if (~up)
            begin
                case(q)
                    3'b000: q <= s7;
                    3'b001: q <= s0;
                    3'b011: q <= s1;
                    3'b010: q <= s2;
                    3'b110: q <= s3;
                    3'b111: q <= s4;
                    3'b101: q <= s5;
                    3'b100: q <= s6;
                    default: q <= q;
                endcase
            end//else
    end // always_comb

endmodule // ex4_38